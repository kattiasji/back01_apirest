package mis.pruebas.apirest.controlador;

import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;

@RestController
@RequestMapping(Rutas.CUENTAS)
public class ControladorCuenta {

    @Autowired
    ServicioCuenta servicioCuenta;

    // GET http://localhost:8080/api/v1/cuenta -> List<Cuenta> obtenerCuentas()
    @GetMapping
    public List<Cuenta> obtenerCuentas() {
        return this.servicioCuenta.obtenerCuentas();
    }

    // POST http://localhost:8080/api/v1/cuentas + DATOS -> agregarCuenta(Cuenta)
    @PostMapping
    public void agregarCuenta(@RequestBody Cuenta cuenta) {
        this.servicioCuenta.insertarCuentaNuevo(cuenta);
    }

    // GET http://localhost:8080/api/v1/cuentas/{nrodecta} -> obtenerUnaCuenta(nrodecta)
    // GET http://localhost:8080/api/v1/cuentas/12345678 -> obtenerUnaCuenta("12345678")
    @GetMapping("/{numerocta}")
    public Cuenta obtenerUnaCuenta(@PathVariable String numerocta) {
        try {
            return this.servicioCuenta.obtenerCuenta(numerocta);
        }catch (Exception x){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    // PUT http://localhost:8080/api/v1/cuentas/{nrodecta} + DATOS -> reemplazarUnaCuenta(nrodecta, DATOS)
    // PUT http://localhost:8080/api/v1/cuentas/12345678 + DATOS -> reemplazarUnaCuenta("12345678", DATOS)
    @PutMapping("/{numerocta}")
    public void reemplazarUnaCuenta(@PathVariable("numerocta") String nrodecta,
                                    @RequestBody Cuenta cuenta) {
        try {
            cuenta.numerocta = nrodecta;
            this.servicioCuenta.guardarCuenta(cuenta);
        }catch (Exception x){
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

    // PUT http://localhost:8080/api/v1/cuentas/{numerocta} + DATOS -> emparcharUnaCuenta(numerocta, DATOS)
    // PUT http://localhost:8080/api/v1/cuentas/12345678 + DATOS -> emparcharUnaCuenta("12345678", DATOS)
    @PatchMapping("/{numerocta}")
    public void emparacharUnaCuenta(@PathVariable("numerocta") String nroCuenta,
                                    @RequestBody Cuenta cuenta) {
        cuenta.numerocta = nroCuenta;
        this.servicioCuenta.emparcharCuenta(cuenta);
    }

    // DELETE http://localhost:8080/api/v1/cuentas/{numerocta} -> eliminarUnCliente(numerocta)
    // DELETE http://localhost:8080/api/v1/cuentas/12345678 + DATOS -> eliminarUnaCuenta("12345678")
    @DeleteMapping("/{numerocta}")
    @ResponseStatus(HttpStatus.OK)
    public void eliminarUnaCuenta(@PathVariable String numerocta){
        try {
            this.servicioCuenta.borrarCuenta(numerocta);
        } catch (Exception x) {

        }
    }

}
