package mis.pruebas.apirest.controlador;

import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.Servicios.ServicioCliente;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(Rutas.CLIENTES + "/{documento}/cuentas")
public class ControladorCuentasCliente {

    // CRUD - GET *,GET,POST,PUT,PATCH

    @Autowired
    ServicioCliente servicioCliente;

    // POST http://localhost:8080/api/v1/clientes/12345678/cuentas + DATOS -> agregarCuentaCliente(documento,cuenta)
    @PostMapping
    public ResponseEntity agregarCuentaCliente(@PathVariable String documento,
                                               @RequestBody Cuenta cuenta) {
        try {
            this.servicioCliente.agregarCuentaCliente(documento, cuenta);
        } catch(Exception x) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.ok().build();
    }


    // GET http://localhost:8080/api/v1/clientes/12345678/cuentas -> obtenerCuentasCliente(documento)
    @GetMapping
    public ResponseEntity<List<Cuenta>> obtenerCuentasCliente(@PathVariable String documento) {
        try {
            final List<Cuenta> cuentasCliente = this.servicioCliente.obtenerCuentasCliente(documento);
            return ResponseEntity.ok(cuentasCliente);
        } catch(Exception x) {
            return ResponseEntity.notFound().build();
        }
    }

    // DELETE http://localhost:8080/api/v1/clientes/12345678/{documento}/cuentas -> eliminarUnClienteCuenta(documento)
    // DELETE http://localhost:8080/api/v1/clientes/12345678 + DATOS -> eliminarUnClienteCliente("12345678")
    @DeleteMapping("/{numerocta}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void eliminarCuentaCliente(@PathVariable String numerocta) {
        try {
            this.servicioCliente.borrarCliente(numerocta);
        } catch(Exception x) {}
    }
}
