package mis.pruebas.apirest.Servicios.impl;

import mis.pruebas.apirest.modelos.Cuenta;
import mis.pruebas.apirest.servicios.ServicioCuenta;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class ServicioCuentaImpl implements ServicioCuenta {

    public final Map<String,Cuenta> cuentas = new ConcurrentHashMap<String,Cuenta>();

    @Override
    public List<Cuenta> obtenerCuentas() {
        return List.copyOf(this.cuentas.values());
    }

    @Override
    public void insertarCuentaNuevo(Cuenta cuenta) {
        this.cuentas.put(cuenta.numerocta, cuenta);
    }

    @Override
    public Cuenta obtenerCuenta(String numerocta) {
        if (!this.cuentas.containsKey(numerocta))
            throw new RuntimeException("No existe la cuenta: " + numerocta);
        return this.cuentas.get(numerocta);
    }

    @Override
    public void guardarCuenta(Cuenta cuenta) {
        if (!this.cuentas.containsKey(cuenta.numerocta))
            throw new RuntimeException("No existe el cliente: " + cuenta.numerocta);
        this.cuentas.replace(cuenta.numerocta, cuenta);
    }

    @Override
    public void emparcharCuenta(Cuenta parche) {
        final Cuenta existente = this.cuentas.get(parche.numerocta);

        if(parche.numerocta != existente.numerocta)
            existente.numerocta = parche.numerocta;

        if(parche.moneda != null)
            existente.moneda = parche.moneda;

        if(parche.saldo != null)
            existente.saldo = parche.saldo;

        if(parche.tipo != null)
            existente.tipo = parche.tipo;

        if(parche.estado != null)
            existente.estado = parche.estado;

        if(parche.oficina != null)
            existente.oficina = parche.oficina;

        this.cuentas.replace(existente.numerocta, existente);
    }

    @Override
    public void borrarCuenta(String numerocta) {
        if (!this.cuentas.containsKey(numerocta))
            throw new RuntimeException("No existe el cliente: " + numerocta);
        this.cuentas.remove(numerocta);
    }
}
