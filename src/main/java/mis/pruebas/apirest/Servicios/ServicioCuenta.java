package mis.pruebas.apirest.servicios;

import mis.pruebas.apirest.modelos.Cuenta;

import java.util.List;

public interface ServicioCuenta {

    // CRUD

    public List<Cuenta> obtenerCuentas();

    // CREATE
    public void insertarCuentaNuevo(Cuenta cuenta);

    // READ
    public Cuenta obtenerCuenta(String nrodecta);

    // UPDATE (solamente modificar, no crear).
    public void guardarCuenta(Cuenta cuenta);
    public void emparcharCuenta(Cuenta parche);

    // DELETE
    public void borrarCuenta(String nrodecta);
}